#pragma once
#include "AvlTreeNode.h"

class AvlTree
{
private:
	AvlTreeNode *root;

	void destroy(AvlTreeNode* leaf); //zniszcz
	int height(AvlTreeNode *node); //daj wyskokosc node
	int diff(AvlTreeNode *node); //daj roznice wyskokosci od node
	AvlTreeNode *rrRotation(AvlTreeNode *up);
	AvlTreeNode *llRotation(AvlTreeNode *up);
	AvlTreeNode *lrRotation(AvlTreeNode *up);
	AvlTreeNode *rlRotation(AvlTreeNode *up);
	AvlTreeNode* balance(AvlTreeNode *node); //zbalansuj node
	void insert(AvlTreeNode *leaf, int data); //dodaj elem
	void display(AvlTreeNode *node, int level); //wyswietl drzewo
	void inOrder(AvlTreeNode *node);
	void preOrder(AvlTreeNode *node);
	void postOrder(AvlTreeNode *node);
	AvlTreeNode* remove(AvlTreeNode* node, int data); //usu� elem
	AvlTreeNode* removeMin(AvlTreeNode* leaf); //minimum przneie� do g�ry
	AvlTreeNode* findMin(AvlTreeNode* leaf); //odnjd� minimum
	void AvlTree::treeBalance(); //zbalansuj drzewo

public:
	AvlTree() : root(nullptr) {};
	~AvlTree();

	void destroy();
	void remove(int data);
	int height();
	void insert(int data);
	void display();
	void inOrder();
	void preOrder();
	void postOrder();
};
