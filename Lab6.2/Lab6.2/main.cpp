#include "AvlTree.h"
#include <iostream>
#include <cstdlib>

int main()
{
	AvlTree a;

	a.insert(1);
	a.insert(2);
	a.insert(3);
	a.insert(4);
	a.insert(5);
	a.insert(6);
	a.insert(7);
	a.insert(8);
	a.insert(9);
	a.insert(25);
	a.insert(36);
	a.insert(20);
	a.insert(22);
	a.insert(10);
	a.insert(12);
	a.insert(30);
	a.insert(28);
	a.insert(40);
	a.insert(38);
	a.insert(48);

	a.display();
	std::cout << "\nWysokosc drzewa:" << a.height() << "\n";

	a.remove(36);

	a.display();
	std::cout << "\nWysokosc drzewa:" << a.height() << "\n";

	system("PAUSE");
}