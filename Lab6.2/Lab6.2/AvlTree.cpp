#include "AvlTree.h"
#include <iostream>

//dekonstruktor
AvlTree::~AvlTree()
{
	destroy();
}

//zniszczenie ca�ego drzewa rekurencyjnie
void AvlTree::destroy(AvlTreeNode* leaf)
{
	if (leaf != nullptr)
	{
		destroy((*leaf).getLeft());
		destroy((*leaf).getRight());
		delete leaf;
	}
}

//Wysoko�� AVL
int AvlTree::height(AvlTreeNode *node)
{
	int h = 0;
	if (node != nullptr)
	{
		int h_left = height((*node).getLeft());
		int h_right = height((*node).getRight());
		int max_height = [h_left, h_right]()->int { return ((h_left > h_right) ? h_left : h_right); }();
		h = max_height + 1;
	}
	return h;
}

//r�nica wysoko�ci
int AvlTree::diff(AvlTreeNode *node)
{
	int h_left = height((*node).getLeft());
	int h_right = height((*node).getRight());
	int difference = h_left - h_right;
	return difference;
}

//Rotacja right-right
AvlTreeNode *AvlTree::rrRotation(AvlTreeNode *up)
{
	AvlTreeNode *temp;
	temp = (*up).getRight();
	(*up).setRight((*temp).getLeft());
	(*temp).setLeft(up);
	return temp;
}

//rotacja left-left
AvlTreeNode *AvlTree::llRotation(AvlTreeNode *up)
{
	AvlTreeNode *temp;
	temp = (*up).getLeft();
	(*up).setLeft((*temp).getRight());
	(*temp).setRight(up);
	return temp;
}

//rotacja left-right
AvlTreeNode *AvlTree::lrRotation(AvlTreeNode *up)
{
	AvlTreeNode *temp;
	temp = (*up).getLeft();
	(*up).setLeft(rrRotation(temp));
	return llRotation(up);
}

//rotacja right-left
AvlTreeNode *AvlTree::rlRotation(AvlTreeNode *up)

{
	AvlTreeNode *temp;
	temp = (*up).getRight();
	(*up).setRight(llRotation(temp));
	return rrRotation(up);
}

//metoda balansuj�ca drzewo
AvlTreeNode *AvlTree::balance(AvlTreeNode *node)
{
	//zbalansowanie dzieciaczk�w
	if ((*node).getLeft() != nullptr)
		(*node).setLeft(balance((*node).getLeft()));
	if ((*node).getRight() != nullptr)
		(*node).setRight(balance((*node).getRight()));
	
	//wyliczenie r�nicy
	int difference = diff(node);

	//poszczeg�lne przypadki
	if (difference >= 2)
	{
		if (diff((*node).getLeft()) <= -1)
		{
			node = lrRotation(node);
			std::cout << "Dokonano rotacji LR, na wezle o zawartosci: " << (*node).getData() << "\n";
		}
		else
		{
			node = llRotation(node);
			std::cout << "Dokonano rotacji LL, na wezle o zawartosci: " << (*node).getData() << "\n";
		}
	}
	else if (difference <= -2)
	{
		if (diff((*node).getRight()) >= 1)
		{
			node = rlRotation(node);
			std::cout << "Dokonano rotacji RL, na wezle o zawartosci: " << (*node).getData() << "\n";
		}
		else
		{
			node = rrRotation(node);
			std::cout << "Dokonano rotacji RR, na wezle o zawartosci: " << (*node).getData() << "\n";
		}
	}
	return node;
}

//zbalansowanie drzewa -> przypisanie nowego node'a.
void AvlTree::treeBalance()
{
	AvlTreeNode *new_root = balance(root);
	if (new_root != root)
		root = new_root;
}

//dodaj element
void AvlTree::insert(AvlTreeNode *leaf, int data)
{
	if (data < (*leaf).getData())
	{
		if ((*leaf).getLeft() != nullptr)
			insert((*leaf).getLeft(), data);
		else
		{
			AvlTreeNode *newElem = new AvlTreeNode;
			(*newElem).setData(data);
			(*leaf).setLeft(newElem);
			(*newElem).setFather(leaf);
		}
	}
	else if (data > (*leaf).getData())
	{
		if ((*leaf).getRight() != nullptr)
			insert((*leaf).getRight(), data);
		else
		{
			AvlTreeNode *newElem = new AvlTreeNode;
			(*newElem).setData(data);
			(*leaf).setRight(newElem);
			(*newElem).setFather(leaf);
		}
	}
	else
	{
		std::cerr << "Nie mozna dodac drugiego takiego samego elementu\n";
		return;
	}

	treeBalance();
}

//usuni�cie elementu o warto�ci data z drzewa
AvlTreeNode* AvlTree::remove(AvlTreeNode* node, int data)
{
	if (node == nullptr) 
		return 0;

	//iteruj tak d�ugo, a� znajdziesz element o danym kluczu
	if (data < (*node).getData())
		(*node).setLeft(remove((*node).getLeft(), data));
	else if (data > (*node).getData())
		(*node).setRight(remove((*node).getRight(), data));

	else //gdy znajdziesz
	{
		AvlTreeNode* left_leaf = (*node).getLeft();
		AvlTreeNode* right_leaf = (*node).getRight();
		delete node;

		if (right_leaf == nullptr) 
			return left_leaf;

		AvlTreeNode* min = findMin(right_leaf);//znajd� min
		(*min).setRight(removeMin(right_leaf));//przyczep po prawej to co uzyskamy w prawej
		(*min).setLeft(left_leaf);//przyczep po lewej lewy li��

		return balance(min);//zbalansuj min
	}
	return balance(node);
}

//printuj
void AvlTree::display(AvlTreeNode *node, int level)
{
	int i;
	if (node != nullptr)
	{
		display((*node).getRight(), level + 1);
		std::cout << "\n";

		if (node == root)
			std::cout << "->";

		for (i = 0; i < level && node != root; i++)
			std::cout << "  ";

		std::cout << (*node).getData();
		display((*node).getLeft(), level + 1);
	}
}

//InOrder
void AvlTree::inOrder(AvlTreeNode *node)
{
	if (node == nullptr)
		return;
	inOrder((*node).getLeft());
	std::cout << (*node).getData() << "  ";
	inOrder((*node).getRight());
}

//PreOrder
void AvlTree::preOrder(AvlTreeNode *node)
{
	if (node == nullptr)
		return;
	std::cout << (*node).getData() << "  ";
	preOrder((*node).getLeft());
	preOrder((*node).getRight());
}

//PostOrder
void AvlTree::postOrder(AvlTreeNode *node)
{
	if (node == nullptr)
		return;
	postOrder((*node).getLeft());
	postOrder((*node).getRight());
	std::cout << (*node).getData() << "  ";
}
//___________________________________________________________________
//Metody dla usuwania
AvlTreeNode* AvlTree::removeMin(AvlTreeNode* leaf)
{
	if ((*leaf).getLeft() == 0)
		return (*leaf).getRight();
	(*leaf).setLeft(removeMin((*leaf).getLeft()));
	return balance(leaf);
}

AvlTreeNode* AvlTree::findMin(AvlTreeNode *leaf) 
{
	return ((*leaf).getLeft() != nullptr) ? findMin((*leaf).getLeft()) : leaf;
}
//_____________________________________________________________________
//Metody usera
int AvlTree::height()
{
	return height(root);
}

void AvlTree::insert(int data)
{
	if (root == nullptr)//je�li nie istnieje root
	{
		AvlTreeNode *newElem = new AvlTreeNode;
		(*newElem).setData(data);
		root = newElem;
		balance(root);
	}
	else//w przeciwnym wypadku
		insert(root, data);

	return;
}

void AvlTree::remove(int data)
{
	root = remove(root, data);
	return;
}

//printuje drzewo, a nast�pnie wyskoo�ci i r�nice mi�dzy nimi
void AvlTree::display()
{
	display(root, 1);

	std::cout << "\n";
	int h_left = height((*root).getLeft());
	std::cout << "Wysokosc lewego poddrzewa: " << h_left << "\n";
	int h_right = height((*root).getRight());
	std::cout << "Wysokosc prawego poddrzewa: " << h_right << "\n";
	int difference = h_left - h_right;
	std::cout << "Roznica wysokosci: " << difference << "\n";

	return;
}

void AvlTree::inOrder()
{
	inOrder(root);
	return;
}

void AvlTree::preOrder()
{
	preOrder(root);
	return;
}

void AvlTree::postOrder()
{
	postOrder(root);
	return;
}

void AvlTree::destroy()
{
	destroy(root);
}