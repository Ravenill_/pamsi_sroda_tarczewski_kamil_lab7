#pragma once

class AvlTreeNode
{
private:
	int data;
	AvlTreeNode *father;
	AvlTreeNode *left;
	AvlTreeNode *right;

public:
	AvlTreeNode() : father(nullptr), left(nullptr), right(nullptr) {}

	int getData() { return data; }
	void setData(int value) { data = value; }
	AvlTreeNode* getLeft() { return left; }
	void setLeft(AvlTreeNode* node) { left = node; }
	AvlTreeNode* getRight() { return right; }
	void setRight(AvlTreeNode* node) { right = node; }
	AvlTreeNode* getFather() { return father; }
	void setFather(AvlTreeNode* node) { father = node; }
};