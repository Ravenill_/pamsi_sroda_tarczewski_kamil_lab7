#pragma once
#include "HashNode.h"
//Linking Probe

class HashMap {

private:
	HashNode **tab;//podw�jny wska�nik, poniewa� tak mo�emy wskaza� na pusty element
	HashNode *free;
	int size;

public:
	HashMap();
	HashMap(int s);
	~HashMap();

	int get(int key);
	void put(int key, int data);
	void remove(int key);
};

bool isPrime(int num);