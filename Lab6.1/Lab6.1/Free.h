#pragma once
#include "HashNode.h"

class Free : public HashNode
{
private:
	static Free *entry;
	Free() : HashNode(-1, -1) {}

public:
	static Free *set()
	{
		if (entry == nullptr)
			entry = new Free();
		return entry;
	}
};
Free *Free::entry = nullptr;