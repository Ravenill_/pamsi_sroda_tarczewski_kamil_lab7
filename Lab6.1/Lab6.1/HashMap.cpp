//Linking Probe
#include "HashMap.h"
#include <iostream>

HashMap::HashMap()
{
	free = new HashNode(-1, -1);
	
	std::cout << "Podaj wielkosc tablicy: \n";
	std::cin >> size;
	std::cout << "Wielkosc tablicy " << ((isPrime(size)) ? "" : "NIE") << " jest liczba pierwsza\n";

	tab = new HashNode*[size]; //wska�nik na tablic� wska�nik�w wskazuj�c� na HashNode
	for (int i = 0; i < size; i++)
		tab[i] = nullptr;
}

HashMap::HashMap(int s)
{
	free = new HashNode(-1, -1);

	size = s;
	std::cout << "Wielkosc tablicy " << ((isPrime(size)) ? "" : "NIE") << " jest liczba pierwsza\n";

	tab = new HashNode*[size]; //wska�nik na tablic� wska�nik�w wskazuj�c� na HashNode
	for (int i = 0; i < size; i++)
		tab[i] = nullptr;
}

HashMap::~HashMap()
{
	for (int i = 0; i < size; i++)
		if (!(tab[i] == nullptr || tab[i] == free))
			delete tab[i];
	delete[] tab;

	delete free;
}

int HashMap::get(int key)
{
	int probe = 1;
	int hash = key % size;

	if (tab[hash] == nullptr) //je�li pusta
	{
		std::cerr << "Nie znaleziono elementu\n";
		return -1;
	}
	else //je�li nie, to szukaj
	{
		for (int i = 0; (tab[hash] != nullptr) && ((*tab[hash]).getKey() != key); i++, hash = (hash + 1) % size)
		{
			probe++;
			if (i > size)//je�li ca�a tablica wype�niona
			{
				std::cerr << "Element o podanym kluczu NIE ISTNIEJE\n";
				return -1;
			}
		}
		std::cout << "Ilosc probek: " << probe << "\n";
		return (*tab[hash]).getData();
	}
}

void HashMap::put(int key, int data)
{
	int probe = 1;
	int hash = key % size;
	HashNode *newelem = new HashNode (key, data);

	if (tab[hash] == nullptr || tab[hash] == free) //je�li pusta to wstaw
		tab[hash] = newelem;
	else //je�li nie, to szukaj pierwszego pustego
	{
		for (int i = 0; (tab[hash] != nullptr) && ((*tab[hash]).getKey() != key); i++, hash = (hash + 1) % size)
		{
			probe++;
			if (i > size)//je�li ca�a tablica wype�niona
			{
				std::cerr << "TABLICA JEST PELNA\n";
				delete newelem;
				return;
			}
		}
		delete tab[hash];
		tab[hash] = newelem;
	}
	std::cout << "Ilosc probek: " << probe << "\n";
}

void HashMap::remove(int key)
{
	int hash = key % size;

	if (tab[hash] == nullptr) //je�li pusta
	{
		std::cerr << "Nie znaleziono elementu\n";
		return;
	}
	else //je�li nie, to szukaj
	{
		for (int i = 0; (tab[hash] != nullptr) && ((*tab[hash]).getKey() != key); i++, hash = (hash + 1) % size)
			if (i > size)//je�li ca�a tablica wype�niona
			{
				std::cerr << "Nie mozna usunac elementu - NIE ISTNIEJE\n";
				return;
			}
		delete tab[hash];
		tab[hash] = free;
	}
}

bool isPrime(int num)
{
	for (int i = 2; i < num; i++)
		if (num % i == 0)
			return false;
	return true;
}