#include "HashMap.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

int main()
{
	srand(static_cast<unsigned long>(time(NULL)));

	int *tab;
	int size;
	std::cout << "Podaj ilosc danych, ktore chcesz wprowadzic: \n";
	std::cin >> size;

	HashMap hash(size);
	tab = new int[size * 2];

	tab[0] = rand() % (size * 5);
	tab[1] = rand() % (size * 5);
	for (int i = 0, j = 0; i < size; i++, j = j + 2)
	{
		do
		{
			bool test = 1;
			int temp1 = rand() % (size * 5);
			int temp2 = rand() % (size * 5);

			for (int z = 0; z < j; z++)
			{
				if (tab[z] == temp1 || tab[z] == temp2 || temp1 == temp2)
					test = 0;
			}

			if (test)
			{
				tab[j] = temp1;
				tab[j + 1] = temp2;
				break;
			}
		} while (true);
		std::cout << "Wylosowane wartosci - klucz: " << tab[j] << ", dane: " << tab[j + 1] << "\n";
		hash.put(tab[j], tab[j + 1]);
	}

	for (int i = 0; i < size * 2; i = i + 2)
		std::cout << hash.get(tab[i]) << "\n";

	delete[] tab;
	system("PAUSE");
}
/*
int main()
{
HashMap hash;

std::cout << "\nDodanie elementu o kluczu 12 i zawartosci 1\n";
hash.put(12, 1);
std::cout << "\nDodanie elementu o kluczu 14 i zawartosci 2\n";
hash.put(14, 2);
std::cout << "\nDodanie elementu o kluczu 16 i zawartosci 3\n";
hash.put(16, 3);
std::cout << "\n";
system("PAUSE");

std::cout << "\nWyswietl element o kluczu 12\n";
std::cout << hash.get(12);
std::cout << "\nWyswietl element o kluczu 14\n";
std::cout << hash.get(14);
std::cout << "\nWyswietl element o kluczu 11\n";
std::cout << hash.get(11);
std::cout << "\n";
system("PAUSE");

std::cout << "\nUsun element o kluczu 12\n";
hash.remove(12);
std::cout << "\nWyswietl element o kluczu 12\n";
std::cout << hash.get(12);
std::cout << "\n";
system("PAUSE");

std::cout << "\nWyswietl element o kluczu 14\n";
std::cout << hash.get(14);
std::cout << "\n";
system("PAUSE");

std::cout << "\nUsun element o kluczu 11\n";
hash.remove(11);
std::cout << "\n";
system("PAUSE");

std::cout << "\nUsun element o kluczu 14\n";
hash.remove(14);
std::cout << "\nWyswietl element o kluczu 14\n";
std::cout << hash.get(14);
std::cout << "\n";
system("PAUSE");
}
*/