#pragma once
#include "HashNode.h"
#include "dList.h"
//Linkowanie

class HashMap {

private:
	dList<HashNode*> *tab;
	int size;

public:
	HashMap();
	HashMap(int s);
	~HashMap();

	int get(int key);
	void put(int key, int data);
	void remove(int key);
};

bool isPrime(int num);