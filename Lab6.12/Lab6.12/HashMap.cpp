//Linkowanie
#include "HashMap.h"
#include <iostream>

HashMap::HashMap()
{
	std::cout << "Podaj wielkosc tablicy: \n";
	std::cin >> size;
	std::cout << "Wielkosc tablicy " << ((isPrime(size)) ? "" : "NIE") << " jest liczba pierwsza\n";

	tab = new dList<HashNode*>[size]; //wska�nik na list� wska�nik�w wskazuj�c� na HashNode
}

HashMap::HashMap(int s)
{
	size = s;
	std::cout << "Wielkosc tablicy " << ((isPrime(size)) ? "" : "NIE") << " jest liczba pierwsza\n";

	tab = new dList<HashNode*>[size]; //wska�nik na list� wska�nik�w wskazuj�c� na HashNode
}

HashMap::~HashMap()
{
	for (int i = 0; i < size; i++)
		while (tab[i].rozmiar() > 0)
			tab[i].usunPierwszy();
	delete[] tab;
}

int HashMap::get(int key)
{
	int hash = key % size;

	if (tab[hash].rozmiar() <= 0) //je�li pusta
	{
		std::cerr << "Nie znaleziono elementu\n";
		return -1;
	}
	else //je�li nie, to szukaj
	{
		int val = 0;
		for (int i = 0; i < tab[hash].rozmiar(); i++)
			if ((*(*(tab[hash]).wez(i)).getData()).getKey() == key)
				return (*(*(tab[hash]).wez(i)).getData()).getData();
		std::cerr << "Nie znaleziono elementu\n";
		return -1;
	}
}

void HashMap::put(int key, int data)
{
	int hash = key % size;
	HashNode* newelem = new HashNode (key, data);

	tab[hash].wstawOstatni(newelem);
}

void HashMap::remove(int key)
{
	int hash = key % size;

	if (tab[hash].rozmiar() <= 0) //je�li pusta
	{
		std::cerr << "Nie znaleziono elementu\n";
		return;
	}
	else //je�li nie, to szukaj
	{
		for (int i = 0; i < tab[hash].rozmiar(); i++)
			if ((*(*(tab[hash]).wez(i)).getData()).getKey() == key)
			{
				(tab[hash]).remove(i);
				return;
			}
	}
}

bool isPrime(int num)
{
	for (int i = 2; i < num; i++)
		if (num % i == 0)
			return false;
	return true;
}