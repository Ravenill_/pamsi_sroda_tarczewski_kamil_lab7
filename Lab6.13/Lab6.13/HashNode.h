#pragma once

class HashNode
{
private:
	int key;
	int data;

public:
	HashNode() : key(0), data(0){}
	HashNode(int key_, int value) : key(key_), data(value){}

	int getKey() const { return key; }
	int getData() const { return data; }
	void setData(int value) { data = value; }
};