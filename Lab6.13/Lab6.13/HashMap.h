#pragma once
#include "HashNode.h"
//Dual Hashing

class HashMap {

private:
	HashNode **tab;//podw�jny wska�nik, poniewa� tak mo�emy wskaza� na pusty element
	HashNode *free;
	int size;

public:
	HashMap();
	HashMap(int s);
	~HashMap();

	int get(int key);
	void put(int key, int data);
	void remove(int key);
	int getSize() const { return size; }
};

bool isPrime(int num);
int primeReversed(int num);